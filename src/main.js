// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Router from "vue-router";
import routes from './router/index.js'

Vue.config.productionTip = false;

const customRouter = new Router({
  routes
});

customRouter.beforeEach((to, from, next) => {
  let token = localStorage.token;

  if (token) {
    if (isLogin) {
      next();
    }
    else {
      next({name: 'layout'})
    }
  }
  else if (!token && localStorage.page !== 'login') {
    localStorage.page = 'login';
    next({name: 'auth.login'})
  }
  else {
    next();
  }
});

export default customRouter

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
