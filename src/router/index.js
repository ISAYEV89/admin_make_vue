import Vue from 'vue'
import Router from 'vue-router'
//import home from '@/components/home'
import login from '../components/login'
import signup from '../components/signup'
import layout from '../page/layout'
import dashboard from '../page/dashboard'
import pricingtable from '../page/PricingTable'
import buttons from '../page/elements/buttons'
import Vuelidate from 'vuelidate'

import animations from '../page/elements/animations'
import components from '../page/elements/components'
import helper from '../page/elements/helper'
import icons from '../page/elements/icons'
import modals from '../page/elements/modals'
import nestable from '../page/elements/nestable'
import notifications from '../page/elements/notifications'
import portlets from '../page/elements/portlets'
import tabs from '../page/elements/tabs'
import treeview from '../page/elements/treeView'
import typography from '../page/elements/typography'
import page404 from '../page/page404'

import builder from '../components/builder';
import newMessage from "../components/newMessage";



Vue.component('builder', builder);
Vue.component('newMessage', newMessage);

Vue.use(Router);
Vue.use(Vuelidate);

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: login,
      meta: {
        isLogin: true
      },
    },
    {
      path: '/signup',
      name: 'signup',
      component: signup,

    },
    {
      path: '/',
      component: layout,
      name: 'layout',
      meta: {
        isLogin: true
      },
      children: [
        // {path: '/', name: 'dashboard', component: dashboard},
        {path: '/pricingtable', name: 'pricingtable', component: pricingtable},
        {path: '/buttons', name: 'buttons', component: buttons},
        {path: '/animations', name: 'animations', component: animations},
        {path: '/components', name: 'components', component: components},
        {path: '/icons', name: 'icons', component: icons},
        {path: '/helper', name: 'helper', component: helper},
        {path: '/modals', name: 'modals,', component: modals},
        {path: '/nestable', name: 'nestable', component: nestable},
        {path: '/notifications', name: 'notifications', component: notifications},
        {path: '/portlets', name: 'portlets', component: portlets},
        {path: '/tabs', name: 'tabs', component: tabs},
        {path: '/treeview', name: 'treeview', component: treeview},
        {path: '/typography', name: 'typography', component: typography},
      ],
    },
    {path: '/page404', component: page404},
    {path: '*', redirect: "/page404"},
  ],
  mode: 'history',
});

